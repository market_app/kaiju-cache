"""
Abstract caching classes.

How to implement your own caching service
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You should to inherit from :class:`.AbstractCache` and implement all required
methods. You probably will need to use :class:`kaiju_tools.services.Service` or
:class:`kaiju_tools.services.ContextableService` to make your cache usable by
the service context manager.

If you expect to use Redis-like store which stores data as plain text, be sure
to provide encoding/decoding of arbitrary data. You could use `kaiju_tools.serialization.dumps`
and `kaiju_tools.serialization.loads` to convert between python data and JSON.

Here is an example of how to implement your custom cache usable by other services.

.. code-block:: python

    from my_some_cache import Connector

    from kaiju_cache.abc import AbstractCache
    from kaiju_tools.services import ContextableService
    from kaiju_tools.serialization import dumps, loads


    class AbstractCache(ContextableService, AbstractCache):

        encoder = dumps
        decoder = loads

        def __init__(self, app, host, port, logger=None)
            super().__init__(app=app, logger=logger)
            self._host = host
            self._port = port
            self._connection = None

        async def init(self):
            self._connection = await Connector.connect(host, port)

        async def close(self):
            await self._connection.close()
            self._connection = None

        @property
        def closed(self):
            return self.connection is None or self.connection.closed

        async def set(self, key, value, ttl=None):
            value = self.encoder(value)
            await self._connection.set_some_key(key, value, ttl)

        async def mset(self, **keys):
            for key, value in keys.items():
                ... # do the same as in `set` ideally in a transaction block

        ... # other methods should be implemented as well, see `AbstractCache`


Classes
-------

"""

import abc
from typing import Optional

__all__ = ('AbstractCache',)


class AbstractCache(abc.ABC):
    """Interface for all cache classes."""

    encoder = None      # use this to encode your data (if required)
    decoder = None      # use this to decode cache value (if required)

    @abc.abstractmethod
    async def exists(self, key: str) -> bool:
        """Checks if key is present in the cache."""

    @abc.abstractmethod
    async def get(self, key: str) -> Optional:
        """Get value of a key or None if not found."""

    @abc.abstractmethod
    async def mget(self, *keys: str) -> list:
        """Get value of multiple keys."""

    @abc.abstractmethod
    async def set(self, key: str, value, ttl: int = None):
        """
        Set a single key at once.

        :param ttl: expiration value in milliseconds, 0 or None should count
            for "infinity"
        """

    @abc.abstractmethod
    async def mset(self, **keys: str):
        """Set one or multiple keys at once."""

    @abc.abstractmethod
    async def delete(self, *keys: str):
        """Remove one or multiple keys at once."""
