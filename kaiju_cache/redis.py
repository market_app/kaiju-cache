from hashlib import sha1
from typing import Optional

import aredis

from kaiju_tools.encoding import serializers, MimeTypes
from kaiju_tools.services import ContextableService

from .abc import AbstractCache

__all__ = ['RedisCache']


class RedisCache(aredis.StrictRedis, ContextableService, AbstractCache):
    """
    Interface for redis caching backend.

    Basically it's compatible with :class:`.abc.AbstractCache`, but it also
    implements all :class:`aredis.StrictRedis` methods as well.

    .. caution::

        Value type compatibility is achieved using JSON, so ensure that you are
        using JSON compatible types here.

    """

    service_name = 'redis'

    def __init__(self, app, *args, default_serializer=MimeTypes.msgpack, logger=None, **kws):
        ContextableService.__init__(self, app=app, logger=logger)
        aredis.StrictRedis.__init__(self, *args, **kws)
        self._closed = True
        self._serializer = serializers[default_serializer]

    async def init(self):
        await self.info()
        self._closed = False

    async def close(self):
        self._closed = True

    @property
    def closed(self) -> bool:
        return self._closed

    # other AbstractCache methods are present in StrictRedis base class

    async def get(self, key: str, json=True) -> Optional:
        value = await super().get(key)
        if value:
            if json:
                return self._serializer.loads(value)
            else:
                return value.decode('utf-8')

    async def mget(self, *keys: str, json=True) -> dict:
        values = await super().mget(*keys)
        result = {}
        if json:
            for k, v in zip(keys, values):
                if v is not None:
                    result[k] = self._serializer.loads(v)
        else:
            for k, v in zip(keys, values):
                if v is not None:
                    result[k] = v.decode('utf-8')
        return result

    async def set(self, key: str, value, ttl: int = None, json=True):
        if json:
            value = self._serializer.dumps_bytes(value)
        else:
            value = str(value)
        await super().set(key, value)
        if ttl:
            await super().pexpire(key, ttl)

    def mset(self, json=True, **keys):
        if json:
            keys = {k: self._serializer.dumps_bytes(v) for k, v in keys.items()}
        else:
            keys = {k: str(v) for k, v in keys.items()}
        return super().mset(**keys)
