from .fixtures import *
from ..services import *


async def _test_cache(cache, logger):

    logger.info("Testing basic operations")

    key = 'test'
    await cache.set(key, key, ttl=1000)
    _value = await cache.get(key)
    assert _value == key
    await cache.delete(key)
    _value = await cache.exists(key)
    assert _value is False

    logger.info("Testing bulk operations")

    key1, key2 = 'test1', 'test2'
    await cache.mset(**{key1: key1, key2: key2})
    _value1, _value2 = await cache.mget(key1, key2)
    assert _value1 == key1
    assert _value2 == key2

    logger.info("Testing json dumps")

    await cache.set(key, {'data': True})
    result = await cache.get(key)
    assert result['data'] is True


async def _test_redis_specific_functions(cache, logger):

    logger.info('Testing scripting')

    await cache.set('some_key', 1)

    script = 'return redis.call("exists", KEYS[1])'
    sha = await cache.load_script(script)
    v = await cache.execute_script(sha, ['some_key'])
    assert v


async def test_redis_cache(redis, redis_cache, logger):
    await _test_cache(redis_cache, logger=logger)
    await _test_redis_specific_functions(redis_cache, logger=logger)


async def test_keydb_cache(keydb, redis_cache, logger):
    await _test_cache(redis_cache, logger=logger)
    await _test_redis_specific_functions(redis_cache, logger=logger)


async def test_local_cache(logger):
    cache = LocalCache()
    await _test_cache(cache, logger=logger)

