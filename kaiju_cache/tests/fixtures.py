"""
Place your pytest fixtures here.
"""

import pytest

from kaiju_tools.tests.fixtures import *

from ..services import RedisCache, LocalCache

REDIS_PORT = 6399


@pytest.fixture
def redis_cache(logger):
    redis = RedisCache(
        app=None, logger=logger,
        host='localhost', port=REDIS_PORT)
    return redis


@pytest.fixture
def local_cache(logger):
    cache = LocalCache(app=None, logger=logger)
    return cache


def _keydb_container(container_function):
    return container_function(
        image='eqalpha/keydb',
        name='keydb',
        version='latest',
        ports={'6379': str(REDIS_PORT)},
        sleep=1,
        healthcheck={
            'test': "echo 'INFO' | redis-cli", 'interval': 100000000,
            'timeout': 3000000000, 'start_period': 1000000000, 'retries': 3
        }
    )


def _redis_container(container_function):
    return container_function(
        image='redis',
        name='redis',
        version='latest',
        ports={'6379': str(REDIS_PORT)},
        sleep=1,
        healthcheck={
            'test': "echo 'INFO' | redis-cli", 'interval': 100000000,
            'timeout': 3000000000, 'start_period': 1000000000, 'retries': 3
        }
    )


@pytest.fixture
def redis(container):
    """
    Returns a new database container. See `kaiju_tools.tests.fixtures.container`
    for more info.
    """

    return _redis_container(container)


@pytest.fixture
def keydb(container):
    return _keydb_container(container)


@pytest.fixture(scope='session')
def per_session_redis(per_session_container):
    """
    Returns a new database container. See `kaiju_tools.tests.fixtures.per_session_container`
    for more info.
    """

    return _redis_container(per_session_container)


@pytest.fixture
def per_session_keydb(container):
    return _keydb_container(per_session_container)
