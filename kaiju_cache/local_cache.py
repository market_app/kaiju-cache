"""
Caching classes that provide the same interface specified in :class:`.abc.AbstractCache`.

Currently available backends are: local (dictionary based) and Redis. See
class descriptions for more info.

Usage
-----

Usage is simple. There are get, set, delete and exists methods present. See more
info in classes documentation.

.. code-block:: python

    cache = LocalCache()
    await cache.set('key', 'value', 1)
    if await cache.exists('key'):
        v = await cache.get('key')  # if key doesn't exist it will return None


Classes
-------

"""

from typing import Optional

from kaiju_tools.services import Service
from kaiju_tools.ttl_dict import TTLDict

from .abc import AbstractCache

__all__ = ['LocalCache']


class LocalCache(Service, AbstractCache):
    """Proxy to a TTLDict object mocking a real cache."""

    service_name = 'local_cache'

    def __init__(self, app=None, logger=None):
        super().__init__(app=app, logger=logger)
        self._cache = TTLDict()

    async def exists(self, key: str) -> bool:
        """Checks if key is present in the cache."""

        return key in self._cache

    async def get(self, key: str) -> Optional:
        """Get value of a key."""

        return self._cache.get(key)

    async def mget(self, *keys: str) -> list:
        """Get value of multiple keys."""

        return [self._cache.get(key) for key in keys]

    async def set(self, key: str, value, ttl: int = None):
        """
        Set a single key at once.

        :param ttl: expiration value in milliseconds, 0 or None should count
            for "infinity"
        """

        return self._cache.set(key, value, ttl=ttl)

    async def mset(self, **keys):
        """Set one or multiple keys at once."""

        for key, value in keys.items():
            self._cache.set(key, value, ttl=0)

    async def delete(self, *keys: str):
        """Remove one or multiple keys at once."""

        for key in keys:
            del self._cache[key]
